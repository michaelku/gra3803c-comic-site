$(document).ready(function() {
	/*Initialize animation to prevent animation glitching*/
	$('#char_1').animate({ right: 120 }, 0);
	$('#char_2').animate({ right: 80 }, 0);
	$('#char_3').animate({ right: 40 }, 0);
	$('#char_4').animate({ right: 0 }, 0);
	
	/*Initial page hide*/
	$('.bio').hide();
	$('.gallery').hide();
	$('#page_1').show();

	/*Bio Navigation*/
	$('.slide .button a').click(function(e){
		e.preventDefault();
		var clicked = $(this).attr('rel');
		
		if(clicked) {
			if(clicked == 1){
				$('#char_1').animate({ right: 120 },1000);
				$('#char_2').animate({ right: 80 }, 1000);
				$('#char_3').animate({ right: 40 }, 1000);
				
				$('.bio').slideUp(1000);
				$('#page_1').slideDown(1000);
			}
			
			else if(clicked == 2){
				$('#char_1').animate({ right: 940 },1000);
				$('#char_2').animate({ right: 80 }, 1000);
				$('#char_3').animate({ right: 40 }, 1000);
				
				$('.bio').slideUp(1000);
				$('#page_2').slideDown(1000);
			}
			
			else if(clicked == 3){
				$('#char_1').animate({ right: 940 },1000);
				$('#char_2').animate({ right: 900 }, 1000);
				$('#char_3').animate({ right: 40 }, 1000);
				
				$('.bio').slideUp(1000);
				$('#page_3').slideDown(1000);
			}
			
			else if(clicked == 4){
				$('#char_1').animate({ right: 940 },1000);
				$('#char_2').animate({ right: 900 }, 1000);
				$('#char_3').animate({ right: 860 }, 1000);
				
				$('.bio').slideUp(1000);
				$('#page_4').slideDown(1000);
			}
		}
	});
	
	/*Gallery Navigation*/
	$('#gallery a').click(function(e){
		e.preventDefault();
		var clicked = $(this).attr('rel');
		
		if(clicked) {		
			$('.gallery').slideUp(1000);
			$('#page_'+clicked+'').slideDown(1000);
		}
	});
});